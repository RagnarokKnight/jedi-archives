# Jedi Archives

Curate and review all kinds of files.

File organizer with collaborative, crowd-sourced distributed metadata database and auto-tagging.

#### Feature Ideas
* File manager with tags instead of hierarchical structure.
* Implement Both web and desktop.
* Desktop similar to [spacedrive](https://www.spacedrive.app/) or [tagspaces](https://www.tagspaces.org/). It would be simpler to use default applications to run files.
* Web it would be nice to self-host and allow users to access online without installing. Could be built with Rust, Actix, Diesel, Inferno and Typescript. Same as [lemmy](https://github.com/LemmyNet/lemmy).
* Have other fields besides tags, like dates, etc.
* Be able to filter with an advanced search. Using multiple fields, logical operators, regex, etc. Search syntax like[beets](https://beets.readthedocs.io/) or [mtg.wtf](https://mtg.wtf/help/syntax).
* All files are scanned and tagged with a shared metadata database. For scanning and tagging see [wutag](https://github.com/vv9k/wutag).
* By default the metadata for every tagged file is automatically uploaded to a shared database. [OrbitDB](https://orbitdb.org/).
* Changes to the database are submitted and people vote to approve or reject them. Like in [stash-box](https://github.com/stashapp/stash-box).
* There is a score based piramid-like structure to grant moderation privileges to users. Like stackoverflow.
* There are reviews for each file with points like in reddit.
* Scrape data from provided links for a file, like number of reviews. Add them if there are multiple sources.
* Allow users to make collections, vote them etc. Collections can be downloaded with one click.
* Ability to set naming conventions including find and replace. Like [mnamer](https://pypi.org/project/mnamer/).
* Duplicate finder, perceptual hashes, fuzzy hashes. [czkawka](https://github.com/qarmin/czkawka).
* Group users with similar interests and provide content suggestion.
* Automatically seed files with the least health.
* Ideally cryptographic signatures tied to the crowd sourced metadata, probably using a partial trust approach. Ideally using a trust network in order to establish weights to be associated with proposed metadata.

  I need a history of edits for each modification but I don't know how to express that.

Add each change to a document as a new record in a collection that represents the history of that document.
Primary key is a time stamp.

u/Silver_Ad_6874

  My two cents, since I've been contemplating something similar for a while now for a file indexer:

  Use Postgresql. Make the meta data JSON formatted. This gives you a lot of initial freedom/leverage to tinker with the data model. In the end, it might even turn out to be a definitive solution for very type specific metadata (exif, geo tags, review content, etc.) It also makes it easy to add functionality incrementally. Once you start hitting performance issues, you renormalize, or earlier once parts start to crystallize out.

Not sure if you are committed to SQL, but TerminusDB, an immutable store, seems to meet your design parameters: https://github.com/terminusdb/terminusdb

#### Useful projects:
- [watchit](https://github.com/ZorrillosDev/watchit-app): open movies everywhere. JavaScript, SCSS. AGPL-3.0 license 
- [record](https://bafybeier5retyjd2y2h4242fxvb73apixr5gg4w47wynsaaojghyeskfvm.ipfs.infura-ipfs.io/) a distributed audio file system. JavaScript. MIT license. [repo](https://reddit.com/r/musichoarder/comments/lrqx7m/record_a_distributed_audio_file_system/)
- [tagspaces](https://www.tagspaces.org/) organize your files and folders with tags and colors. TypeScript, Java, JavaScript. AGPL-3.0. [repo](https://github.com/tagspaces/tagspaces)
- [OrbitDB](https://orbitdb.org/): Peer-to-Peer Databases for the Decentralized Web. JavaScript. MIT license. [repo](https://github.com/orbitdb/orbit-db)
- [beets](https://beets.readthedocs.io/), the media library management system for obsessive music geeks. Python. MIT license. [repo](https://github.com/beetbox/beets)
- [stash](https://stashapp.cc/), an organizer for your porn. Go, GraphQL. AGPL-3.0 license. [repo](https://github.com/stashapp/stash)
- [stash-box](https://github.com/stashapp/stash-box)
- [calibre](https://calibre-ebook.com/), ebook manager. Python, C. GNU General Public License. [repo](https://github.com/kovidgoyal/calibre)
- [lib.reviews](https://lib.reviews/), a free/libre code and information platform for reviews of anything. Web, JavaScript, Handlebars. CC0 1.0 Universal. [repo](https://github.com/eloquence/lib.reviews)
- [etiquette](https://github.com/voussoir/etiquette), tag-based file organizer & search. Web: Python Flask, SQLite3. BSD-3-Clause license.
- [mtg.wtf](https://mtg.wtf/help/syntax), mtg card advanced search. Haml, Ruby. MIT License. [repo](https://github.com/taw/magic-search-engine)
- [Supertag](https://amoffat.github.io/supertag/): a tag-based filesystem. [reddit post](https:/www.reddit.com/r/rust/comments/k5n0fs/i_made_a_tagbased_filesystem_in_rust/). rust. AGPL-3.0 license. [repo](https://github.com/amoffat/supertag)
- [TMSU](https://tmsu.org/) lets you tag your files and then access them through a nifty virtual filesystem from any other application. Go, Shell. GNU General Public License version 3. [repo](https://github.com/oniony/TMSU)
- [tagf](https://github.com/elkhanib/tagf), tag your files and folders to make them easier to find. Go. Apache-2.0 license
- [tocc](https://tocc.aidinhut.com/), a Tool for Obsessive Compulsive Classifiers. C++. GPL-3.0 license. [repo](https://github.com/aidin36/tocc)
- [tagfs](https://github.com/harshhemani/tagfs), Tag based file manager written in python (Currently a CLI). Python. GPL-3.0 license.
- [carpo](https://github.com/LarryHsiao/carpo_rs), A tool to tag and search files. Rust, CSS. MIT license.
- [wutag](https://github.com/vv9k/wutag), CLI tool for tagging and organizing files by tag. Rust. MIT license.
- [czkawka](https://github.com/qarmin/czkawka), multi functional app to find duplicates, empty folders, similar images, etc. Rust, Fluent. MIT License.a
- [spacedrive](https://www.spacedrive.app/), file explorer from the future. TypeScript, Rust. GPL-3.0 license. [repo](https://github.com/spacedriveapp/spacedrive)
- [mnamer](https://pypi.org/project/mnamer/), media file renamer and organizion tool. Python. MIT license. [repo](https://github.com/jkwill87/mnamer)
- NextCloud
- owncloud
