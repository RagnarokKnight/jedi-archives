use dotenv;
use mongodb::{bson::doc, sync::Client};

pub fn db_test() -> mongodb::error::Result<()> {
    dotenv::dotenv().ok();
    let db_url = std::env::var("DATABASE_URL").unwrap();

    // Get a handle to the cluster
    let client = Client::with_uri_str(db_url)?;
    // Ping the server to see if you can connect to the cluster
    client
        .database("admin")
        .run_command(doc! {"ping": 1}, None)?;
    println!("Connected successfully.");
    // List the names of the databases in that cluster
    for db_name in client.list_database_names(None, None)? {
        println!("{}", db_name);
    }
    Ok(())
}

// Create database schema

// Populate local database from public one

// Database CRUD


/*
A collaborative, crowd-sourced, distributed database of metadata,
something like a distributed musicbrainz.
When a user edits his metadata the edit can be approved by
voting on it by the other users, and when edits are approved
they are pulled to the other databases.

Since there are public ids associated to each user it is easy
to make the voting system. I would only have to count votes
for an edit in the database from each local node.

Now what if I wanted to have a list of moderator ids in the database
so that they could approve or reject submissions skipping the voting.

@draeder-5d1167f3d73408ce4fc468a0:gitter.im
Create a user or SEA pair for your app, then log in as that user or with that SEA pair, and put the moderators to that user's graph. Only the app can modify the list.

@rococtz:matrix.org
What draeder said + use SEA.certify so only those users can vote (or tell the people listening "only trust these people") but I feel it's easier to tell them "trust everybody who can vote" and make sure only your people can vote using SEA.certify
*/

